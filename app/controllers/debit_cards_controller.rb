# frozen_string_literal: true

class DebitCardsController < ApplicationController
  before_action :authenticate_user!

  def new
    @debit_card = current_user.default_debit_card
    @new_debit_card = current_user.debit_cards.build
  end

  def create
    prepare_debit_card
    respond_to do |format|
      make_payment
      format.js
    end
  end

  private def prepare_debit_card
    if params[:existing_card]
      @debit_card = current_user.default_debit_card
    else
      submission = DebitCardSubmission.new(
        current_user,
        debit_card_params.merge(card_number: params[:card_number])
      )
      if submission.valid?
        begin
          @debit_card = DebitCardCreator.new(submission).perform!
        rescue ActiveRecord::RecordInvalid => error
          @alert = error.record.errors.messages.values.join('\n')
        end
      else
        @alert = submission.errors.messages.values.join('\n')
      end
    end
  end

  private def make_payment
    return if @alert.present?
    result = @debit_card.stub_perform_payment(params[:amount])
    if result.to_i == 100
      @notice = 'You order has been completed!'
    else
      @alert = "The specified debit card number is invalid. Please check the number you've provided and try again"
    end
  end

  private def debit_card_params
    params.require(:card).permit(:cvv, :exp_month)
  end
end
