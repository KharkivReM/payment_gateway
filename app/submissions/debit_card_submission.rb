# frozen_string_literal: true

require 'bcrypt'

class DebitCardSubmission
  include ActiveModel::Model

  attr_reader :user, :token, :card_number, :last4, :exp_month, :exp_year
  attr_accessor :cvv

  validates :card_number, presence: { message: 'Card number must be present' }
  validates :user, presence: { message: 'User must be present' }
  validate :expiration_date_valid?
  validate :debit_card_number_valid?
  validate :unique_token?

  def initialize(user, attrs = {})
    @user = user
    @card_number = attrs.delete(:card_number)
    @last4 = @card_number && @card_number[-4..-1]
    @exp_year, @exp_month = attrs.delete(:exp_month).try(:split, '-')
    tokenize if card_number
    super(attrs)
  end

  private def tokenize
    @token = BCrypt::Engine.hash_secret(card_number, Settings.encryption.bcrypt_salt)
  end

  private def expiration_date_valid?
    unless exp_month && exp_year
      errors.add('exp_month', 'Expiration Date is Invalid')
      return false
    end
    current_month = Time.zone.today.month
    current_year = Time.zone.today.year
    if current_year > exp_year.to_i || (current_year == exp_year.to_i && current_month > exp_month.to_i)
      errors.add('exp_month', 'Expiration Date needs to be in the future')
      return false
    end
  end

  private def debit_card_number_valid?
    unless CreditCardValidations::Luhn.valid?(card_number)
      errors.add('card_number', 'Card Number is invalid') && false
    end
  end

  private def unique_token?
    if (debit_card = DebitCard.find_by(token: token)) && debit_card.user != user
      errors.add('card_number', 'An error occurred while processing the card')
      false
    end
  end
end
