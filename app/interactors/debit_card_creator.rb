# frozen_string_literal: true

class DebitCardCreator
  attr_accessor :debit_card_submission

  def initialize(debit_card_submission)
    self.debit_card_submission = debit_card_submission
  end

  def perform!
    DebitCard.transaction do
      user.debit_cards.each(&:make_non_default!)
      DebitCard.create!(
        user:                 user,
        token:                debit_card_submission.token,
        last4:                debit_card_submission.last4,
        exp_month:            debit_card_submission.exp_month,
        exp_year:             debit_card_submission.exp_year,
        cvv:                  debit_card_submission.cvv
      )
    end
  end

  private def user
    @user ||= debit_card_submission.user
  end
end
