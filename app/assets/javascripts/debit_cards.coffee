# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
  $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );

$ ->
  $('#new_card_form').submit ->
    cleanErrors()
    check_format($('#card_number'))
    check_format($('#card_cvv'))
    check_length($('#card_number'), 16)
    check_length($('#card_cvv'), 3)
    check_expiration_date($('#card_exp_month'))
    return (($('.error_message').filter () -> !!this.innerText).length is 0)

addError = (message, input) ->
  element = input.nextAll('.error_message')[0]
  if not element.innerText
    element.append(message)

cleanError = (input) ->
  input.nextAll('.error_message').empty()

check_format = (input) ->
  isInteger = /^\d+$/
  if not isInteger.test(input.val())
    addError("Must be a number", input)

check_length = (input, required_length) ->
  if not (input.val().length is required_length)
    addError("Must be " + required_length + " digits", input)

check_expiration_date = (input) ->
  if input.val().length is 0
    addError("Must be present", input)
    return false
  exp_date = input.val().split '-'
  exp_year = parseInt(exp_date[0], 10)
  exp_month = parseInt(exp_date[1], 10)
  today = new Date
  current_month = today.getMonth() + 1 
  current_year = today.getFullYear()
  if (current_year > exp_year) || (current_year is exp_year  && current_month > exp_month)
    addError("Expiration date needs to be in the future", input)

cleanErrors = () ->
  cleanError($('#card_number'))
  cleanError($('#card_cvv'))
  cleanError($('#card_exp_month'))
