# frozen_string_literal: true

require 'bcrypt'

class DebitCard < ApplicationRecord
  attr_encrypted :cvv, encode: true, encode_iv: true, encode_salt: true

  belongs_to :user
  validates :token, presence: { message: 'Token must be present' }
  validates :token, uniqueness: { scope: %i[exp_month exp_year], message: 'The card was declined' }
  validates :user, presence: { message: 'User must be present' }

  scope :default, -> { where(default_flg: true) }
  after_initialize { |record| record.default_flg = true }

  def make_non_default!
    self.default_flg = false
    save!
  end

  # Stub out payment call to Stripe. Single action instead of auth/capture process
  # 100 - Your payment has been accepted.
  # 203 - Your payment has been declined. Please re-check
  # with the providing bank
  def stub_perform_payment(_amount)
    card_number = '4242424242424242'
    token == BCrypt::Engine.hash_secret(card_number, Settings.encryption.bcrypt_salt) ? '100' : '203'
  end
end
