# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :validatable

  has_many :debit_cards

  validates :email,
            uniqueness: true,
            format: { with: email_regexp },
            allow_blank: true,
            allow_nil: true

  validates :first_name, :last_name, presence: true

  def default_debit_card
    debit_cards.default.first
  end
end
