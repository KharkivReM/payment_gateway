class CreateDebitCards < ActiveRecord::Migration[5.0]
  def change
    create_table :debit_cards do |t|
      t.integer :user_id
      t.string :token
      t.integer :exp_month
      t.integer :exp_year
      t.string :last4
      t.string :encrypted_cvv
      t.boolean :default_flg

      t.timestamps
    end
    add_foreign_key :debit_cards, :users
    add_index :debit_cards, :user_id
    add_index(:debit_cards, [:token, :exp_month, :exp_year], {unique: true, name: "debit_cards__token_exp_month_exp_year"})
  end
end
