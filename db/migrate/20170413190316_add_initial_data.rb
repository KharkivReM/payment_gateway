require 'core_data_update'

class AddInitialData < ActiveRecord::Migration[5.0]
  include CoreDataUpdate

  def up
    apply_update :"0000_initial_data_update"
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
