class InitialDataUpdate < SeedMigrator::Updater
  def perform_update
    user1_without_card = User.create!(
      email:                  'john@test.com',
      password:               'test123',
      password_confirmation:  'test123',
      first_name:             'John',
      last_name:              'Smith'
    )
    user2_without_card = User.create!(
      email:                  'mark@test.com',
      password:               'test123',
      password_confirmation:  'test123',
      first_name:             'Mark',
      last_name:              'James'
    )
    user3_without_card = User.create!(
      email:                  'paul@test.com',
      password:               'test123',
      password_confirmation:  'test123',
      first_name:             'Paul',
      last_name:              'Johnson'
    )
    user1_with_card = User.create!(
      email:                  'steve@test.com',
      password:               'test123',
      password_confirmation:  'test123',
      first_name:             'Steve',
      last_name:              'Stevenson'
    )
    user1_with_card.debit_cards.create!(
      token:      'fake token',
      exp_month:  '10',
      exp_year:   '2020',
      last4:      '5432',
      cvv:        '123'
    )
    user2_with_card = User.create!(
      email:                  'jack@test.com',
      password:               'test123',
      password_confirmation:  'test123',
      first_name:             'Jack',
      last_name:              'Madison'
    )
    user2_with_card.debit_cards.create!(
      token:      'fake token2',
      exp_month:  '9',
      exp_year:   '2015',
      last4:      '7895',
      cvv:        '123'
    )
  end

  def undo_update
    raise ActiveRecord::IrreversibleMigration
  end
end
