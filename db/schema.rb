# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170413190316) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "debit_cards", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "token"
    t.integer  "exp_month"
    t.integer  "exp_year"
    t.string   "last4"
    t.string   "encrypted_cvv"
    t.boolean  "default_flg"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["token", "exp_month", "exp_year"], name: "debit_cards__token_exp_month_exp_year", unique: true, using: :btree
    t.index ["user_id"], name: "index_debit_cards_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",              default: "", null: false
    t.string   "encrypted_password", default: "", null: false
    t.string   "first_name",                      null: false
    t.string   "last_name",                       null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

  add_foreign_key "debit_cards", "users"
end
