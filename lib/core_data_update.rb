# frozen_string_literal: true

module CoreDataUpdate
  include SeedMigrator

  def root_updates_path
    Rails.root.join('db', 'data_updates')
  end

  def should_run?(_update_name)
    true
  end
end
