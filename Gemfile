# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.2'
# DB
gem 'pg', '~> 0.20'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
#
# jQuery UI for the Rails asset pipeline
gem 'jquery-ui-rails'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# Provides a clean way to handle updates to seed data post-launch
gem 'seed_migrator', github: 'KharkivReM/seed_migrator', branch: 'rails5'

# Flexible authentication solution for Rails with Warden
gem 'devise'

# environment specific application configuration.
gem 'config'

# Generates attr_accessors that encrypt and decrypt attributes
gem 'attr_encrypted'

# Ruby binding for the OpenBSD bcrypt() password hashing algorithm
gem 'bcrypt', require: false

gem 'credit_card_validations'

group :development, :test do
  # Debugging
  gem 'pry', '~> 0.10.4', require: false
  gem 'pry-rails', '~> 0.3.6'

  # Tidy
  gem 'rubocop', '~> 0.48', require: false
  gem 'rubocop-rspec', '~> 1.15', require: false

  # Testing
  gem 'capybara'
  gem 'capybara-screenshot', require: false
  gem 'cucumber-rails', require: false
  gem 'database_cleaner'
  gem 'factory_girl_rails', '~> 4.8', require: false
  gem 'faker'
  gem 'poltergeist'
  gem 'rspec-activemodel-mocks'
  gem 'rspec-rails', '~> 3.5'
  gem 'timecop', require: false
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen', '~> 3.0.5'
  gem 'web-console', '>= 3.3.0'
end
