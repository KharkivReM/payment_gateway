# frozen_string_literal: true

FactoryGirl.define do
  factory :user do
    first_name              { Faker::Name.first_name }
    last_name               { Faker::Name.last_name  }
    email                   { Faker::Internet.email("#{first_name} #{last_name}") }
    password                'secret123'
    password_confirmation   'secret123'
  end
end
