# frozen_string_literal: true

FactoryGirl.define do
  factory :debit_card do
    token { Faker::Code.ean }
    cvv { '543' }
    last4 { '1999' }
    exp_month { 1 }
    exp_year { Date.today.year + 1 }
    default_flg { true }
  end
end
