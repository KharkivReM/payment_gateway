# frozen_string_literal: true

require 'rails_helper'
require 'bcrypt'

RSpec.describe DebitCardsController, type: :controller do
  let(:user) { FactoryGirl.create :user }
  let(:another_user) { FactoryGirl.create :user }
  let(:existing_debit_card) { FactoryGirl.create(:debit_card, user: user) }
  let(:valid_card_number) { '4242424242424242' }
  let(:invalid_card_number) { '4111111111111111' }
  let(:valid_token) do
    BCrypt::Engine.hash_secret(valid_card_number, Settings.encryption.bcrypt_salt)
  end
  let(:cvv) { '543' }
  let(:exp_month) { 1 }
  let(:exp_year) { Time.zone.today.year + 2 }
  let(:exp_date) { "#{exp_year}-#{exp_month}" }
  let(:parameters) do
    {
      card_number: valid_card_number,
      card: {
        cvv: cvv,
        exp_month: exp_date
      }
    }
  end
  let(:existing_valid_card) do
    FactoryGirl.create(:debit_card, token: valid_token, user: user)
  end
  let(:existing_invalid_card) do
    FactoryGirl.create(:debit_card, user: user)
  end
  let(:another_user_debit_card) do
    FactoryGirl.create :debit_card,
                       token: valid_token,
                       user: another_user
  end

  before do
    allow(request.env['warden']).to receive(:authenticate!).and_return(user)
    allow(controller).to receive(:current_user).and_return(user)
  end

  describe 'GET #new' do
    it 'returns http success' do
      get :new
      expect(response).to have_http_status(:success)
    end

    it 'returns existing debit card if use has it' do
      existing_debit_card
      user.reload
      get :new
      expect(
        controller.instance_eval { @debit_card }
      ).to eq(user.default_debit_card)
    end

    it 'returns empty debit for user without debit cards' do
      get :new
      expect(
        controller.instance_eval { @debit_card }
      ).to be_blank
    end
  end

  describe 'POST #create' do
    it 'returns http success' do
      post :create, params: parameters, format: :js
      expect(response).to have_http_status(:success)
    end

    context 'successfull use of debit card' do
      context 'no existing debit card' do
        it 'creates new debit card' do
          post :create, params: parameters, format: :js
          expect(user.reload.default_debit_card.last4).to eq(valid_card_number[-4..-1])
          expect(user.reload.default_debit_card.exp_month).to eq(exp_month)
          expect(user.reload.default_debit_card.exp_year).to eq(exp_year)
        end

        it 'calls stub_perform_payment' do
          expect_any_instance_of(DebitCard).to receive(:stub_perform_payment)
          post :create, params: parameters, format: :js
        end

        it 'returns successful notice for valid card number' do
          post :create, params: parameters, format: :js
          expect(
            controller.instance_eval { @notice }
          ).to eq('You order has been completed!')
        end
      end

      context 'user with existing debit card' do
        it 'does not create new card' do
          existing_valid_card
          post :create, params: { existing_card: true }, format: :js
          expect(user.reload.debit_cards.count).to eq(1)
        end

        it 'calls stub_perform_payment' do
          existing_valid_card
          expect_any_instance_of(DebitCard).to receive(:stub_perform_payment)
          post :create, params: { existing_card: true }, format: :js
        end

        it 'returns successful notice for valid existing card' do
          existing_valid_card
          post :create, params: { existing_card: true }, format: :js
          expect(
            controller.instance_eval { @notice }
          ).to eq('You order has been completed!')
        end
      end
    end

    context 'invalid data' do
      context 'no existing debit card' do
        it 'returns alert for invalid card number' do
          post :create, params: parameters.merge(card_number: invalid_card_number), format: :js
          expect(
            controller.instance_eval { @alert }
          ).to eq("The specified debit card number is invalid. Please check the number you've provided and try again")
        end

        it 'returns alert for expired card number' do
          post :create, params: parameters.merge(card: { exp_month: '2015-01' }), format: :js
          expect(
            controller.instance_eval { @alert }
          ).to eq('Expiration Date needs to be in the future')
        end

        it 'returns alert for missing card number' do
          post :create, params: parameters.merge(card_number: nil), format: :js
          expect(
            controller.instance_eval { @alert }
          ).to eq('Card number must be present')
        end

        it 'returns alert for card number not passing the luhn check' do
          post :create, params: parameters.merge(card_number: '111'), format: :js
          expect(
            controller.instance_eval { @alert }
          ).to eq('Card Number is invalid')
        end

        it 'does not allow duplicate cards' do
          existing_valid_card
          post :create, params: parameters.merge(
            card: { exp_month: "#{existing_valid_card.exp_year}-#{existing_valid_card.exp_month}" }
          ), format: :js
          expect(
            controller.instance_eval { @alert }
          ).to eq('The card was declined')
        end

        it "does not allow to use other user's card" do
          another_user_debit_card
          post :create, params: parameters, format: :js
          expect(
            controller.instance_eval { @alert }
          ).to eq('An error occurred while processing the card')
        end
      end

      context 'user with existing debit card' do
        it 'returns alert for existing invalid card number' do
          existing_invalid_card
          post :create, params: { existing_card: true }, format: :js
          expect(
            controller.instance_eval { @alert }
          ).to eq("The specified debit card number is invalid. Please check the number you've provided and try again")
        end
      end
    end
  end
end
