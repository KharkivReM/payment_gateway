# frozen_string_literal: true

require 'rails_helper'
require 'timecop'

RSpec.describe DebitCardSubmission do
  let(:user) { FactoryGirl.create :user }
  let(:another_user) { FactoryGirl.create :user }
  let(:parameters) { {} }
  let(:card_number) { '4242424242424242' }

  context 'with blank user' do
    let(:submission) { described_class.new nil, parameters }

    it 'is not valid' do
      expect(submission.valid?).to be_falsy
      expect(submission.errors[:user]).not_to be_empty
    end
  end

  context 'with blank card number' do
    let(:submission) { described_class.new user, parameters }

    it 'is not valid ' do
      expect(submission.valid?).to be_falsy
      expect(submission.errors[:card_number]).not_to be_empty
    end
  end

  context 'with invalid card number' do
    let(:parameters) { { card_number: '1111' } }

    it 'is invalid with bad card number' do
      submission = described_class.new user, parameters
      expect(submission.valid?).to be_falsy
      expect(submission.errors[:card_number].first).to eq('Card Number is invalid')
    end
  end

  context 'with invalid expiration date' do
    let(:parameters) { { card_number: card_number } }

    it 'is invalid with missing expiration date' do
      submission = described_class.new user, parameters
      expect(submission.valid?).to be_falsy
      expect(submission.errors[:exp_month].first).to eq('Expiration Date is Invalid')
    end

    it 'is invalid with expiration year in a past' do
      parameters[:exp_month] = '2016-01'
      Timecop.freeze(2017, 5, 15) do
        submission = described_class.new user, parameters
        expect(submission.valid?).to be_falsy
        expect(submission.errors[:exp_month].first).to eq('Expiration Date needs to be in the future')
      end
    end

    it 'is invalid with expiration month being a previous month' do
      parameters[:exp_month] = '2017-04'
      Timecop.freeze(2017, 5, 15) do
        submission = described_class.new user, parameters
        expect(submission.valid?).to be_falsy
        expect(submission.errors[:exp_month].first).to eq('Expiration Date needs to be in the future')
      end
    end
  end

  context 'with valid parameters' do
    let(:valid_expiration_date) { "#{Time.zone.today.year + 2}-01" }
    let(:parameters) do
      { card_number: card_number, exp_month: valid_expiration_date }
    end
    let(:submission) { described_class.new user, parameters }
    let(:another_user_debit_card) do
      FactoryGirl.create :debit_card,
                         token: submission.token,
                         user: another_user
    end

    it 'is valid' do
      expect(submission.valid?).to eq(true)
    end

    it 'has a token' do
      expect(submission.token).to be_present
    end

    it "does not allow to use other user's card" do
      another_user_debit_card
      expect(submission.valid?).to be_falsy
      expect(submission.errors[:card_number].first).to eq('An error occurred while processing the card')
    end
  end
end
