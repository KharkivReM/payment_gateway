# frozen_string_literal: true

require 'rails_helper'

describe DebitCardCreator do
  let(:user) { FactoryGirl.create :user }
  let(:token) { Faker::Code.ean }
  let(:last4) { '0572' }
  let(:cvv) { '123' }
  let(:exp_month) { 10 }
  let(:exp_year) { 2019 }
  let(:interactor) { described_class.new(submission) }
  let(:submission) do
    mock_model(DebitCardSubmission,
               user:                 user.reload,
               token:                token,
               last4:                last4,
               exp_month:            exp_month,
               exp_year:             exp_year,
               cvv:                  cvv)
  end

  context 'user without debit card' do
    before { interactor.perform! }

    it 'succesfully creates new debit card' do
      expect(user.reload.debit_cards.count).to eq(1)
      debit_card = user.debit_cards.first
      expect(debit_card.token).to eq(token)
      expect(debit_card.last4).to eq(last4)
      expect(debit_card.exp_month).to eq(exp_month)
      expect(debit_card.exp_year).to eq(exp_year)
      expect(debit_card.default_flg).to eq(true)
    end

    it 'does not allow duplicate cards' do
      expect do
        interactor.perform!
      end.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Token The card was declined')
    end
  end

  context 'with existing debit card' do
    let(:existing_debit_card) { FactoryGirl.create(:debit_card, user: user) }

    before do
      existing_debit_card
      interactor.perform!
    end
    it 'marks old debit card as non default' do
      expect(user.debit_cards.first.default_flg).to eq(false)
    end

    it 'marks new debit card as the default debit card' do
      default_debit_card = user.reload.default_debit_card
      expect(default_debit_card.token).to eq(token)
    end
  end
end
