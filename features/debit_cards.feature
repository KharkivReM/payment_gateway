@javascript
Feature: Checkout process
  
  In order to finish checkout process
  As a customer
  I want to provide my debit card information

  Background:
    Given I am signed in

  Scenario: when customer enters new debit card
    When I visit checkout page
    Then I should see an empty form

  Scenario: when customer has debit card
    Given I have a debit card
    When I visit checkout page
    Then I should see my debit card details

  Scenario: when customer enters letters for debit card number
    When I visit checkout page
    And I fill in valid debit card information
    And I enter letters to debit card number field
    And Click submit new debit card button
    Then I should see error message "The specified debit card number is invalid. Please check the number you've provided and try again"

  Scenario: when customer enters expired debit card
    When I visit checkout page
    And I fill in valid debit card information
    And I enter expiration date in the past
    And Click submit new debit card button
    Then I should see error message "Expiration Date needs to be in the future"

  Scenario: when customer enters valid debit card
    When I visit checkout page
    And I fill in valid debit card information
    And Click submit new debit card button
    Then I should see confirmation message

  Scenario: when customer enters not supported debit card
    When I visit checkout page
    And I fill in valid debit card information
    And I fill not supported debit card information
    And Click submit new debit card button
    Then I should see error message "The specified debit card number is invalid. Please check the number you've provided and try again"

  Scenario: when customer enters invalid debit card number
    When I visit checkout page
    And I fill in valid debit card information
    And I enter invalid debit card number
    And Click submit new debit card button
    Then I should see error message "Card Number is invalid"

  Scenario: when customer uses existing valid debit card
    Given I have a valid debit card
    When I visit checkout page
    And Click use debit card
    Then I should see confirmation message

  Scenario: when customer uses existing valid debit card
    Given I have an invalid debit card
    When I visit checkout page
    And Click use debit card
    Then I should see error message "The specified debit card number is invalid. Please check the number you've provided and try again"

  Scenario: when customer creates a duplicate card
    Given I have a valid debit card
    When I visit checkout page
    And I switch to new debit card tab
    And I fill in duplicate card information
    And Click submit new debit card button
    Then I should see error message "The card was declined"

  Scenario: when customer enter somebody else card
    Given Other user has a valid debit card
    When I visit checkout page
    And I fill in valid debit card information
    And I enter not mine debit card
    And Click submit new debit card button
    Then I should see error message "An error occurred while processing the card"
