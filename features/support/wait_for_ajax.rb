module WaitForAjax
  def wait_for_ajax
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until finished_all_ajax_requests?
    end
  end

  def wait_until(timeout=20, poll=1, &block)
    start = Time.now
    while !block.call
      raise Timeout::Error if Time.now - start > timeout
      sleep poll
    end
  end

  def finished_all_ajax_requests?
    page.evaluate_script('jQuery.active').zero?
  rescue Capybara::NotSupportedByDriverError
    true
  end
end

RSpec.configure do |config|
  config.include WaitForAjax
end

World(WaitForAjax)
