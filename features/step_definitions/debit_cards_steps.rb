### GIVEN Statements ###

Given(/^I am signed in$/) do
  @user = create :user
  visit new_user_session_path 
  fill_in "user_email", with: @user.email
  fill_in "user_password", with: 'secret123'
  click_button 'Log in'
end

Given(/^I have a debit card$/) do
  @debit_card = create(:debit_card, user: @user)
end

Given(/^I have a valid debit card$/) do
  valid_card_number = '4242424242424242'
  token = BCrypt::Engine.hash_secret(valid_card_number, Settings.encryption.bcrypt_salt)
  @debit_card = create(:debit_card, token: token, cvv: '123', user: @user)
end

Given(/^I have an invalid debit card$/) do
  invalid_card_number = '4111111111111111'
  token = BCrypt::Engine.hash_secret(invalid_card_number, Settings.encryption.bcrypt_salt)
  @debit_card = create(:debit_card, token: token, user: @user)
end

Given(/^Other user has a valid debit card$/) do
  other_user = create :user
  valid_card_number = '4242424242424242'
  token = BCrypt::Engine.hash_secret(valid_card_number, Settings.encryption.bcrypt_salt)
  @other_debit_card = create(:debit_card,
                             token: token,
                             user: other_user)
end

### WHEN  Statements ###

When(/^I visit checkout page$/) do
	visit new_debit_card_path
end

When(/^I fill in valid debit card information$/) do
  fill_in "card_number", with: '4242424242424242'
  fill_in 'card_cvv', with: '123'
  fill_in 'card_exp_month', with: "#{Date.today.year + 1}-01"
end

When(/^I switch to new debit card tab$/) do
  find('#ui-id-2').click
end

When(/^I fill in duplicate card information$/) do
  fill_in "card_number", with: '4242424242424242'
  fill_in 'card_cvv', with: '123'
  fill_in 'card_exp_month', with: "#{@debit_card.exp_year}-#{@debit_card.exp_month}"
end

When(/^I fill not supported debit card information$/) do
  fill_in "card_number", with: '4111111111111111'
end

When(/^I enter letters to debit card number field$/) do
  fill_in "card_number", with: 'aaaa'
end

When(/^I enter invalid debit card number$/) do
  fill_in "card_number", with: '1111'
end

When(/^I enter expiration date in the past$/) do
  fill_in 'card_exp_month', with: "#{Date.today.year - 1}-01"
end

When(/^I enter not mine debit card$/) do
  fill_in "card_number", with: '4242424242424242'
end

When(/^Click submit new debit card button$/) do
  click_button 'Submit new debit card'
  wait_for_ajax
end

When(/^Click use debit card$/) do
  click_button 'Use debit card'
  wait_for_ajax
end

### THEN Statements ###

Then(/^I should see my debit card details$/) do
	expect(page.find('#existing_card_first_name').value).to eq(@user.first_name)
	expect(page.find('#existing_card_last_name').value).to eq(@user.last_name)
	expect(page.find('#existing_card_number').value).to eq("**** #{@debit_card.last4}")
	expect(page.find('#existing_card_exp_month').value).to eq("#{@debit_card.exp_month}/#{@debit_card.exp_year}")
end

Then(/I should see an empty form$/) do
	expect(page.find('#card_first_name').value).to eq(@user.first_name)
	expect(page.find('#card_last_name').value).to eq(@user.last_name)
	expect(page.find('#card_number').visible?).to eq(true)
	expect(page.find('#card_cvv').visible?).to eq(true)
	expect(page.find('#card_exp_month').visible?).to eq(true)
end

Then(/I should see confirmation message$/) do
  page.should have_content('You order has been completed!')
end

Then('I should see error message "$error_message"') do |error_message|
  page.should have_content(error_message)
end
