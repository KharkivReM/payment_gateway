Rails.application.routes.draw do
  devise_for :users
  resources :debit_cards, only: [:new, :create] 
  root to: 'debit_cards#new'
end
