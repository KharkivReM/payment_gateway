Encryptor.default_options.merge!(
  algorithm: 'aes-256-gcm',
  key: Base64.decode64(Settings.encryption.key),
  iv: Base64.decode64(Settings.encryption.iv),
  salt: Base64.decode64(Settings.encryption.salt)
)
